<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Laravel Soft Delete
Soft Delete adalah fitur dari laravel untuk melakukan penghapusan data sementara. 
Kita bisa menghapus data pada table, tapi data tersebut tidak benar-benar terhapus dari database.
Masih tersimpan dalam table tapi tidak tampil lagi.

Analoginya begini: Dengan fitur soft delete ini ibaratnya kita bisa memasukan data ke tong sampah seperti di Recycle Bin misalnya
di OS Windows. Nah data yang sudah kita masukkan ke tong sampah tersebut bisa kita tampilkan kembali atau bisa jadi kita 
hapus secara permanen.

Misalnya lagi, ibaratnya ada beberapa barang di atas meja. Kita bisa membuang barang-barang tersebut dari atas meja ke tong sampah.
Nah kita masih bisa mengambil barang-barang yang sudah kita buang ke tong sampah kita tadi yang kemudian kita simpan di atas
meja kembali atau kita juga bisa membuangnya secara permanen dengan membakar atau dengan cara yang lainnya.

### Things to do list
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-soft-deletes.git` 
2. Go inside the folder: `cd laravel-soft-deletes`
3. Run `cp .env.example .env` then put your db name with db credentials
4. Run `composer install`
5. Run `php artisan migrate:refresh --seed`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000/guru

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Tong Sampah Page

![Tong Sampah Page](img/trash.png "Tong Sampah Page")
