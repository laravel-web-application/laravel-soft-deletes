CREATE TABLE `guru`
(
    `id`         int(10) UNSIGNED                        NOT NULL PRIMARY KEY,
    `nama`       varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `umur`       int(11)                                 NOT NULL,
    `created_at` timestamp                               NULL DEFAULT NULL,
    `updated_at` timestamp                               NULL DEFAULT NULL,
    `deleted_at` datetime                                     DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `nama`, `umur`, `created_at`, `updated_at`, `deleted_at`)
VALUES (2, 'Fitria Gilda Hastuti S.Pd', 40, NULL, NULL, NULL),
       (3, 'Ana Mayasari S.Gz', 34, NULL, NULL, NULL),
       (4, 'Cemeti Gunarto', 33, NULL, NULL, NULL),
       (5, 'Gangsa Samosir', 41, NULL, NULL, NULL),
       (6, 'Salwa Wastuti', 44, NULL, NULL, NULL),
       (7, 'Cici Oliva Puspita', 37, NULL, NULL, NULL),
       (8, 'Dartono Kusumo', 36, NULL, NULL, NULL),
       (9, 'Hardi Asmadi Uwais S.Ked', 26, NULL, NULL, NULL),
       (10, 'Gina Halimah', 39, NULL, NULL, NULL);


--
ALTER TABLE `guru`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;
